<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\BlogModel;
use App\Models\CommentModel;
use Illuminate\Support\Facades\DB;

class BlogController extends Controller
{
    public function show()
    {
        $data = BlogModel::orderByDesc('created_at')->paginate(10);
        return view('dashboard')->with(['data'=>$data]);
    }

    public function new()
    {
        return view('newpost');
    }

    public function detail($id)
    {
        $data = BlogModel::findorfail($id);
        $comment = CommentModel::where('blog_id', $id)->orderByDesc('created_at')->paginate(10);
        return view('detail')->with(['data'=>$data, 'comment' => $comment]);
    }

    public function save(Request $request)
    {
        $blog = new BlogModel;
        $blog->judul = $request->judul;
        $blog->content = $request->mytextarea;
        $blog->image = $request->file('image')->store('blog-images');
        $blog->save();
        return redirect('/');
    }

    public function update(Request $request, $id)
    {
        BlogModel::where('id', $id)->update(['judul'=>$request->judul, 'content'=> $request->content, 'image' => $request->file('image')->store('blog-images')]);
        return redirect('/');
    }

    public function destroy($id)
    {
        BlogModel::destroy($id);
        // DB::table('blog')->where('id', $id)->delete();
        return redirect()->back();
    }
}
