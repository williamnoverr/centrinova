<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\CommentModel;

class CommentController extends Controller
{
    public function index($id)
    {
        $data = CommentModel::where('blog_id', $id)->orderByDesc('created_at')->paginate(10);
        return view('comment')->with(['data'=>$data]);
    }
    
    public function save(Request $request, $id)
    {
        $save = new CommentModel;
        $save->blog_id = $id;
        $save->nama = $request->nama;
        $save->email = $request->email;
        $save->comment = $request->komentar;
        $save->save();

        return redirect()->back();
    }

    public function destroy($id)
    {
        CommentModel::destroy($id);
        // DB::table('blog')->where('id', $id)->delete();
        return redirect()->back();
    }
}
