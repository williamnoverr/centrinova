<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CommentModel extends Model
{
    use HasFactory;

    protected $table = 'comment';
    protected $fillable = [
        'blog_id',
        'nama',
        'email',
        'comment'
    ];

    public function blog()
    {
        return $this->belongsTo(BlogModel::class, 'blog_id');
    }
}
