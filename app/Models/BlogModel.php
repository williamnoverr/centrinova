<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BlogModel extends Model
{
    use HasFactory;

    protected $table = 'blog';
    protected $fillable = [
        'id',
        'judul',
        'content'
    ];

    protected $appends = [
        'jumlah_comment',
    ];

    public function getJumlahCommentAttribute()
    {
        // $jumlah = $this->comment()->where('', $this->id)->count();
        $jumlah = $this->comment()->count();
        // $obj = new CommentModel;
        // $jumlah = $obj->where('blog_id', $this->id)->count();
        return $jumlah;
    }

    public function comment()
    {
        return $this->hasMany(CommentModel::class, 'blog_id', 'id');
    }
}
