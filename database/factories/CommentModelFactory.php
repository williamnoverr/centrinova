<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class CommentModelFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'blog_id' => random_int(2, 52),
            'nama' => $this->faker->name,
            'email' => $this->faker->safeEmail,
            'comment'  => $this->faker->text
        ];
    }
}
