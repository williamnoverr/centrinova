<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\CommentModel;

class CommentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CommentModel::factory()->count(50)->create();
    }
}
