<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\BlogModel;

class BlogSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        BlogModel::factory()->count(50)->create();
    }
}
