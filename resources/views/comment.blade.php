@extends('layouts.custom-navigation')


@section('container')
<div class="container mt-3">
    <div class="row">
        <table class="table table-borderless table-hover">
            <tr>
                <th style="vertical-align: middle;">Nama</th>
                <th style="vertical-align: middle; text-align: center;">Email</th>
                <th style="vertical-align: middle; text-align: center;">Tanggal</th>
                <th style="vertical-align: middle; text-align: center;">Komentar</th>
                <th style="vertical-align: middle; text-align: center;">Aksi</th>
            </tr>
            @foreach($data as $item)
                <tr>
                    <td style="vertical-align: middle;"><a href="{{ url('detail',$item->id) }}" >{{ $item->nama }}</a></td>
                    <td style="vertical-align: middle; text-align: center;">{{ $item->email }}</td>
                    <td style="vertical-align: middle; text-align: center;">{{ \Carbon\Carbon::parse($item->created_at)->format('d/m/Y h:s')}}</td>
                    <td style="vertical-align: middle;">{{ \Illuminate\Support\Str::limit($item->comment, 50, $end='...') }}</td> 
                    <td style="text-align:center;">
                        <form action="{{ route('delete.comment', $item->id) }}" method="post" class="d-inline">
                            @csrf
                            @method('delete')
                            <button style="margin-right:5px;" class="btn btn-danger" onClick="return confirm('apakah yakin ingin menghapus data ini?')" href="{{ route('delete.comment', $item->id) }}">Delete</button>
                        </form>
                    </td>
                </tr>
            @endforeach
        </table>
        <span>
            {{ $data->links() }}
        </span>
    </div>
</div>
@endsection

<style>
    .w-5{
        display:none
    }
</style>