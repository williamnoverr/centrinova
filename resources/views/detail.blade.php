@extends('layouts.custom-navigation')

@section('container')
<div class="container mt-3">
        @if(!Auth::check())
            <form action="#" method="post">
            @method("put")
            @csrf
                <div class="form-group">
                    <label for="nama">Judul</label><input value ="{{$data->judul}}" type="text" id="nama" name="name" class="form-control" disable>
                </div>
                <div class="form-group">
                    <label for="content">Content</label><textarea type="text" id="content" name="content" class="form-control" disable>{{ $data->content }}</textarea>
                </div>
                @if($data->image)
                <div class="form-group">
                    <img src="{{ asset('storage/'.$data->image) }}" alt="">
                </div>
                @else 
                <div class="form-group">
                    <img src="https://source.unsplash.com/400x400" alt="">
                </div>
                @endif
            </form>
        @else 
            <form action="{{ route('update.blog', $data->id) }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <label for="judul">Judul</label><input value ="{{$data->judul}}" type="text" id="judul" name="judul" class="form-control" >
                </div>
                <div class="form-group">
                    <textarea type="text" id="content" name="content" class="form-control">{{ $data->content }}</textarea>
                    <textarea name="mytextarea" id="mytextarea" class="form-control"></textarea>
                </div>
                @if($data->image)
                <div class="form-group">
                    <img src="{{ asset('storage/'.$data->image) }}" alt="">
                </div>
                @else 
                <div class="form-group">
                    <img src="https://source.unsplash.com/400x400" alt="">
                </div>
                @endif
                <div class="form-group">
                    <input type="file" id="image" name="image" value="{{ $data->image }}">
                </div>
                <div class="form-group">
                    <button class="btn btn-success" type="submit"> Save </button>
                </div>
            </form>
        @endif

    <div>
        @if(!Auth::check())
            <h2 style="margin-top:50px">Tambah Komentar</h2>
            <div>
                <form action="{{ route('comment.save', $data->id)}}" method="post">
                    @csrf
                    <div class="form-group">
                        <label for="nama">Nama</label><input type="text" id="nama" name="nama" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label><input type="email" id="email" name="email" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="komentar">Komentar</label><input type="text" id="komentar" name="komentar" class="form-control">
                    </div>
                    <div class="form-group">
                        <button class="btn btn-success" type="submit"> Save </button>
                    </div>
                </form>
            </div>
        @endif
        <h2 style="margin-top:50px">List Komentar</h2>
        <div class="row">
            <table class="table table-borderless table-hover">
                <tr>
                    <th style="vertical-align: middle;">Nama</th>
                    <th style="vertical-align: middle;">Comment</th>
                    <th style="vertical-align: middle; text-align: center;">Waktu</th>
                </tr>
                @foreach($comment as $item)
                    <tr>
                        <td style="vertical-align: middle;">{{ $item->nama }}</td>
                        <td style="vertical-align: middle;">{{ \Illuminate\Support\Str::limit($item->comment, 20, $end='...') }}</td> 
                        <td style="vertical-align: middle; text-align: center;">{{ \Carbon\Carbon::parse($item->created_at)->format('d/m/Y')}}</td>
                    </tr>
                @endforeach
            </table>
            <span>
                {{ $comment->links() }}
            </span>
        </div>
    </div>
</div>
@endsection

<style>
    .w-5{
        display:none
    }
</style>