@extends('layouts.custom-navigation')


@section('container')
@if(!Auth::check())
<div class="container mt-3">
    <div class="row">
        <table class="table table-borderless table-hover">
            <tr>
                <th style="vertical-align: middle;">Judul</th>
                <th style="vertical-align: middle; text-align: center;">Short Content</th>
                <th style="vertical-align: middle; text-align: center;">Jumlah Comment</th>
            </tr>
            @foreach($data as $item)
                <tr>
                    <td style="vertical-align: middle;"><a href="{{ url('detail',$item->id) }}" >{{ $item->judul }}</a></td>
                    <td style="vertical-align: middle;">{{ \Illuminate\Support\Str::limit($item->content, 100, $end='...') }}</td> 
                    <td style="vertical-align: middle; text-align: center;">{{ $item->jumlah_comment }}</td>
                </tr>
            @endforeach
        </table>
        <span>
            {{ $data->links() }}
        </span>
    </div>
</div>
@else
<div class="container mt-3">
    <div class="row">
        <a class="btn btn-primary" onClick="create()" href="newpost">New Post</a>
        <table class="table table-borderless table-hover">
            <tr>
                <th style="vertical-align: middle; text-align:center;">Judul</th>
                <th style="vertical-align: middle; text-align:center;">Aksi</th>
            </tr>
            @foreach($data as $item)
                <tr>
                    <td style="vertical-align: middle; text-align:center;"><a href="{{ url('detail',$item->id) }}" >{{ $item->judul }}</a></td>
                    <td style="text-align:center;">
                        <a style="margin-right:5px;" class="btn btn-secondary" href="{{ url('detail',$item->id) }}">Edit</a>
                        <form action="{{ route('delete.blog', $item->id) }}" method="post" class="d-inline">
                            @csrf
                            @method('delete')
                            <button style="margin-right:5px;" class="btn btn-danger" onClick="return confirm('apakah yakin ingin menghapus data ini?')" href="{{ route('delete.blog', $item->id) }}">Delete</button>
                        </form>
                        <a class="btn btn-primary" href="{{ url('comment',$item->id) }}">Comment Management</a>
                    </td>
                </tr>
            @endforeach
        </table>
        <span>
            {{ $data->links() }}
        </span>
    </div>
</div>
@endif
@endsection

<style>
    .w-5{
        display:none
    }
</style>