@extends('layouts.custom-navigation')


@section('container')
<div class="container mt-3">
        <!-- @if(session()->has('message'))
            <div class="alert alert-primary" role="alert">
                {{session('message')}}
            </div>
        @endif -->
    <div class="row">
        <form action="{{ route('profile.update')}}" method="post">
            @csrf
            <div class="form-group">
                <label for="nama">Nama</label><input value ="{{Auth::user()->name}}" type="text" id="nama" name="name" class="form-control">
            </div>
            <div class="form-group">
                <label for="email">Email</label><input value ="{{Auth::user()->email}}" type="email" id="email" name="email" class="form-control">
            </div>
            <div class="form-group">
                <label for="password">New Password</label><input type="text" id="password" name="password" class="form-control">
            </div>
            <button class="btn btn-success" type="submit"> Save </button>
        </form>
    </div>
</div>
@endsection