@extends('layouts.custom-navigation')


@section('container')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Editor
                </div>
                <div class="card-body">
                    <form action="{{ route('save.blog')}}" method="post" enctype="multipart/form-data">
                        @csrf 
                        <label for="judul">Judul</label><input type="text" id="judul" name="judul" >
                        <textarea name="mytextarea" id="mytextarea"></textarea>
                        <input type="file" id="image" name="image"><br>
                        <button class="btn btn-success" type="submit">Save</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

