<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\AuthenticatedSessionController;
use App\Http\Controllers\BlogController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\CommentController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/login', [AuthenticatedSessionController::class, 'create'])->name('login');
Route::get('/', [BlogController::class, 'show']);
Route::get('/detail/{id}', [BlogController::class, 'detail'])->name('detail');
Route::post('/save/{id}', [CommentController::class, 'save'])->name('comment.save');

// Profile
Route::get('/profile', [ProfileController::class, 'index'])->middleware(['auth'])->name('profile');
Route::get('/edits-profile', [ProfileController::class, 'edit'])->middleware(['auth'])->name('profile.edit');
Route::post('/update-profile', [ProfileController::class, 'update'])->name('profile.update');


// Blog
Route::get('/dashboard', [BlogController::class, 'show'])->middleware(['auth'])->name('dashboard');
Route::get('/newpost', [BlogController::class, 'new'])->middleware(['auth'])->name('newpost');
Route::post('/save', [BlogController::class, 'save'])->name('save.blog');
Route::delete('/delete/{id}', [BlogController::class, 'destroy'])->middleware(['auth'])->name('delete.blog');
Route::post('/update/{id}', [BlogController::class, 'update'])->name('update.blog');

// Comment Management
Route::get('/comment/{id}', [CommentController::class, 'index'])->middleware(['auth'])->name('comment');
Route::delete('/delete-comment/{id}', [CommentController::class, 'destroy'])->middleware(['auth'])->name('delete.comment');

require __DIR__.'/auth.php';
